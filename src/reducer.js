import { LOCATION_CHANGE } from './constants';

const getInitialState = {
  pathname: '/',
  search: '',
  queries: {},
  hash: '',
};

export const routerReducer = (state = getInitialState, action) => {
  if (action.type === LOCATION_CHANGE) {
    const stateProps = {};
    if (typeof state === 'object') {
      Object.keys(state).forEach((key) => {
        stateProps[key] = state[key];
      });
    }
    if (typeof action.payload === 'object') {
      Object.keys(action.payload).forEach((key) => {
        stateProps[key] = action.payload[key];
      });
    }
    return stateProps;
  }
  return state;
};
